package pl.dzienniczekbobasa;

import pl.dzienniczekbobasa.db.FoodDataSource;
import pl.dzienniczekbobasa.db.UserDataSource;
import pl.dzienniczekbobasa.tabs.MainTabsAdapter;
import pl.dzienniczekbobasa.R;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.pm.ActivityInfo;

/**
 * klasa g��wna
 */
public class MainActivity extends FragmentActivity implements ActionBar.TabListener {
	
	private ViewPager viewPager;
	private MainTabsAdapter mAdapter;
	private ActionBar actionBar;
	private String[] tabs = {null, null, null};
	private UserDataSource uds;
	private FoodDataSource fds;
	private Button foodBreast;
	private Button foodMilk;
	private Button foodDinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		uds = new UserDataSource(getApplicationContext());
		uds.open();
		if(uds.getUserCount()==0) uds.insertUserDefaults();
		
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new MainTabsAdapter(getSupportFragmentManager());
		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		tabs[0] = getResources().getString(R.string.tab_mile_title);
		tabs[1] = getResources().getString(R.string.tab_food_title);
		tabs[2] = getResources().getString(R.string.tab_body_title);
		
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
			}
		
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			 
		    @Override
		    public void onPageSelected(int position) {
		        actionBar.setSelectedNavigationItem(position);
		    }
		 
		    @Override
		    public void onPageScrolled(int arg0, float arg1, int arg2) {
		    }
		 
		    @Override
		    public void onPageScrollStateChanged(int arg0) {
		    }
		});
		viewPager.setCurrentItem(1, false);
		foodBreast = (Button) findViewById(R.id.food_button_breast);
		foodMilk = (Button) findViewById(R.id.food_button_milk);
		foodDinner = (Button) findViewById(R.id.food_button_dinner);
		
		foodButtons();
		
		
	} //koniec onCreate()

	/**
	 * metody wymagane dla funkcjonowania zak�adek
	 */
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {}
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());		}
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {}
	
	/**
	 * metody zgodne z cyklem �ycia aktywno�ci
	 * otwieraj�/zamykaj� po��czenie z baz� danych
	 * podczas pokazywania/ukrywania aktywno�ci
	 */
	@Override
	protected void onPause() {
		uds.close();
		fds.close();
		super.onPause();
		}
	
	@Override
	protected void onResume() {
		uds.open();
		fds.open();
		super.onResume();
		}
	private void showToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}
	/**
	 * obs�uga przycisk�w w zak�adce FOOD
	 */
	private void foodButtons() {
		foodBreast.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showToast(getResources().getString(R.string.food_button_default));
			}});
		foodMilk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showToast(getResources().getString(R.string.food_button_default));
			}});
		foodDinner.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showToast(getResources().getString(R.string.food_button_default));
			}});
		
		foodBreast.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				int def = uds.getDefBreast();
				fds.insertDefaultFood(1, def);
				return true;
			}});
		foodMilk.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				int def = uds.getDefMilk();
				fds.insertDefaultFood(2, def);
				return true;
			}});
		foodDinner.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				int def = uds.getDefDinner();
				fds.insertDefaultFood(3, def);
				return true;
			}});
	} //koniec foodButtons()
}