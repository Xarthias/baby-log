package pl.dzienniczekbobasa.util;

import android.annotation.SuppressLint;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Klasa udost�pnia format zapisu daty i czasu stosowany do zapisu do bazy danych daty
 * i zamiany z typu Date do String i odwrotnie
 */
@SuppressLint("SimpleDateFormat")
public class DateTimeConverter {
	
	private final static String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * Zamiana ci�gu znak�w na obiekt typu Date - format Stringa w zmiennej dateTimeFormat
	 * @param stringDateTime
	 * @return
	 */
	public static Date stringToDate(String stringDateTime){
		SimpleDateFormat formatter = new SimpleDateFormat(dateTimeFormat);
		Date date=new Date();		
		try {
			date = formatter.parse(stringDateTime);
		} catch (ParseException e) {			
			e.printStackTrace();
			return null; // jesli si� nie uda
		}
		return date;
	}

	/**
     * Zamiana obiektu typu Date na ci�g znak�w string, format String w zmiennej
     * dateTimeFormat
     * @param dateTime
     * @return data w stringu jako HH:mm:ss dd-MM-yyyy
     */
    public static String dateToString(Date dateTime) {

        if (dateTime == null)
            return null;

        DateFormat datePattern;
        datePattern = new SimpleDateFormat(dateTimeFormat);
        datePattern.setTimeZone(TimeZone.getTimeZone("GMT+02:00"));
        String date_str = datePattern.format(dateTime);
        return date_str;
    }
	
	/**
	 * Zwraca format daty i czasu wykozystywany przez t� klas�
	 * @return
	 */
	public static String getDateTimeFormat(){
		return dateTimeFormat;
	}

}
