package pl.dzienniczekbobasa.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * klasa tworz�ca baz� danych
 */

public class MyDatabaseHelper extends SQLiteOpenHelper {
	
	/**
	 * Zmienne z nazw� i wersj� bazy
	 */
	private static final String DB_NAME = "dzienniczekbobasa.db";
	private static final int DB_VERSION = 1;
	
	public MyDatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}
	/**
	 * Metoda tworz�ca baz�
	 */
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_T_USER);
//		db.execSQL(CREATE_T_BODY);
		db.execSQL(CREATE_T_FOOD);
//		db.execSQL(CREATE_T_MILE);
	}
	
	/**
	 * Metoda uaktualniaj�ca baz�
	 */
	public void onUpgrade(SQLiteDatabase db, int staraWersja, int nowaWersja) {
		db.execSQL("DROP TABLE IF EXISTS " + T_USER + ";");
//		db.execSQL("DROP TABLE IF EXISTS " + T_BODY + ";");
		db.execSQL("DROP TABLE IF EXISTS " + T_FOOD + ";");
//		db.execSQL("DROP TABLE IF EXISTS " + T_MILE + ";");
		onCreate(db);
	}
	
	/* ---------- Tabela user ---------- */
	/**
	 * Deklaracja tabeli
	 */
	public static final String T_USER = "user";
//	public static final String C_USER_ID = "user_id";
//	public static final String C_USER_NAME = "user_name";
//	public static final String C_USER_DATEOFBIRTH = "user_date_of_birth";
	public static final String C_USER_DEF_BREAST = "user_def_breast";
	public static final String C_USER_DEF_MILK = "user_def_milk";
	public static final String C_USER_DEF_DINNER = "user_def_dinner";
	
	/**
	 * Tablica string�w z nazwami wszystkich kolumn tabeli
	 */
	public static final String[] userAllColumns = {
//						C_USER_ID,
//						C_USER_NAME,
//						C_USER_DATEOFBIRTH,
						C_USER_DEF_BREAST,
						C_USER_DEF_MILK,
						C_USER_DEF_DINNER
						};
	
	/**
	 * Getter nazw tabeli
	 * @return
	 */
	public static String[] getAllColumnsUser() {
		return userAllColumns;
	}
	
	/**
	 * Tworzenie tabeli
	 */
	public static final String CREATE_T_USER = "create table "
//			+ T_USER + "("
//			+ C_USER_ID + " integer primary key autoincrement not null, "
//			+ C_USER_NAME + " text not null, "
//			+ C_USER_DATEOFBIRTH + " text, "
			+ C_USER_DEF_BREAST + " integer not null default 100, "
			+ C_USER_DEF_MILK + " integer not null default 140, "
			+ C_USER_DEF_DINNER + "integer not null default 125"
			+ ");";

//	/* ---------- Tabela bodysize ---------- */
//	public static final String T_BODY = "bodysize";
//	public static final String C_BODY_USERID = "bodysize_user_id";
//	public static final String C_BODY_WEIGHT = "bodysize_weight";
//	public static final String C_BODY_HEIGHT = "bodysize_height";
//	public static final String C_BODY_COMMENT = "bodysize_comment";
//	public static final String C_BODY_SURVEYTIME = "bodysize_surveytime";
//	
//	public static final String[] bodysizeAllColumns = {
//						C_BODY_USERID,
//						C_BODY_WEIGHT,
//						C_BODY_HEIGHT,
//						C_BODY_COMMENT,
//						C_BODY_SURVEYTIME
//						};
//	
//	public static final String[] getAllColumsBody() {
//		return bodysizeAllColumns;
//	}
//	public static final String CREATE_T_BODY = "create table "
//			+ T_BODY + "("
//			+ C_BODY_USERID + " integer not null, "
//			+ C_BODY_WEIGHT + " integer, "
//			+ C_BODY_HEIGHT + " integer, "
//			+ C_BODY_COMMENT + " text, "
//			+ C_BODY_SURVEYTIME + "text"
//			+ ");";
	
	/* ---------- Tabela food ---------- */
	public static final String T_FOOD = "food";
//	public static final String C_FOOD_USERID = "food_userid";
	public static final String C_FOOD_TYPE = "food_type";
	public static final String C_FOOD_QUANTINITY = "food_quantinity";
	public static final String C_FOOD_COMMENT = "food_comment";
	public static final String C_FOOD_SURVEYTIME = "food_surveytime";
	
	public static final String[] foodAllColumns = {
//						C_FOOD_USERID,
						C_FOOD_TYPE,
						C_FOOD_QUANTINITY,
						C_FOOD_COMMENT,
						C_FOOD_SURVEYTIME
						};
	
	public static final String[] getAllColumnsFood() {
		return foodAllColumns;
	}
	
	public static final String CREATE_T_FOOD = "create table "
			+ T_FOOD + "("
//			+ C_FOOD_USERID + " integer not null, "
			+ C_FOOD_TYPE + " integer not null default 1, "
			+ C_FOOD_QUANTINITY + " integer not null, "
			+ C_FOOD_COMMENT + " text, "
			+ C_FOOD_SURVEYTIME + " text"
			+ ");";
	
	/* ---------- Tabela milestones ---------- */
//	public static final String T_MILE = "milestones";
//	
//	public static final String[] mileAllColumns = {
//						
//						};
//	
//	public static final String[] getAllColumnsMile() {
//		return mileAllColumns;
//	}
//	
//	public static final String CREATE_T_MILE = "create table "
//			+ T_MILE + "("
//			
//			+ ");";

}
