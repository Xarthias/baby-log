package pl.dzienniczekbobasa.db.entry;

public class UserEntry {
	private int userDefBreast;
	private int userDefMilk;
	private int userDefDinner;
	
	public int getUserDefBreast() {
		return userDefBreast;
	}
	public void setUserDefBreast(int userDefBreast) {
		this.userDefBreast = userDefBreast;
	}
	public int getUserDefMilk() {
		return userDefMilk;
	}
	public void setUserDefMilk(int userDefMilk) {
		this.userDefMilk = userDefMilk;
	}
	public int getUserDefDinner() {
		return userDefDinner;
	}
	public void setUserDefDinner(int userDefDinner) {
		this.userDefDinner = userDefDinner;
	}
	@Override
	public String toString() {
		return "UserEntry [userDefBreast=" + userDefBreast + ", userDefMilk="
				+ userDefMilk + ", userDefDinner=" + userDefDinner + "]";
	}
	

}
