package pl.dzienniczekbobasa.db;

import java.util.Date;
import pl.dzienniczekbobasa.util.DateTimeConverter;
import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Klasa z metodami otwierającymi i zamykającymi połączenie z bazą,
 * zapisującymi, aktualizującymi, odczytującymi oraz usuwającymi wpisy
 * w tabeli FOOD
 * @author Marcin
 */
public class FoodDataSource {

	private SQLiteDatabase db;
	private MyDatabaseHelper dbHelper;
	
	public FoodDataSource(Context context) {
		dbHelper = new MyDatabaseHelper(context);
	}
	
	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public void insertDefaultFood(int type, int ilosc) {
		ContentValues values = new ContentValues();
		values.put(MyDatabaseHelper.C_FOOD_TYPE, type);
		values.put(MyDatabaseHelper.C_FOOD_QUANTINITY, ilosc);
		values.put(MyDatabaseHelper.C_FOOD_SURVEYTIME, DateTimeConverter.dateToString(new Date()));
		db.insert(MyDatabaseHelper.T_FOOD, null, values);
	}
	
}