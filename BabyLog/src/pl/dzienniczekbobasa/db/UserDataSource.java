package pl.dzienniczekbobasa.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class UserDataSource {
	
	private SQLiteDatabase db;
	private MyDatabaseHelper dbHelper;
	
	public UserDataSource(Context context) {
		dbHelper = new MyDatabaseHelper(context);
	}
	
	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	/**
	 * metoda do ustawienia defaultowych warto�ci porcji jedzonka
	 */
	public void insertUserDefaults() {
		ContentValues values = new ContentValues();
		values.put(MyDatabaseHelper.C_USER_DEF_BREAST, 100);
		values.put(MyDatabaseHelper.C_USER_DEF_MILK, 140);
		values.put(MyDatabaseHelper.C_USER_DEF_DINNER, 125);
		db.insert(MyDatabaseHelper.T_USER, null, values);
	}
	/**
	 * metoda zwracaj�ca ilo�� wierszy w tabeli user
	 * @return
	 */
	public int getUserCount(){
		Cursor c = db.rawQuery("SELECT count(*) from "+MyDatabaseHelper.T_USER, null);
		int count = -1;
		if(c.moveToFirst()) { 
			count = c.getInt(0);
		}		
		return count;	
	}

	/**
	 * @return warto�ci defaultowe karmienia
	 */
	public int getDefBreast() {
		int def = 0;
		Cursor c = db.rawQuery("SELECT " + MyDatabaseHelper.C_USER_DEF_BREAST 
							+ " FROM " + MyDatabaseHelper.T_USER, null);
		if(c.moveToFirst()) {
			def = c.getInt(0);
		}
		return def;
	}
	
	public int getDefMilk() {
		int def = 0;
		Cursor c = db.rawQuery("SELECT " + MyDatabaseHelper.C_USER_DEF_MILK 
							+ " FROM " + MyDatabaseHelper.T_USER, null);
		if(c.moveToFirst()) {
			def = c.getInt(0);
		}
		return def;
	}
	
	public int getDefDinner() {
		int def = 0;
		Cursor c = db.rawQuery("SELECT " + MyDatabaseHelper.C_USER_DEF_DINNER 
							+ " FROM " + MyDatabaseHelper.T_USER, null);
		if(c.moveToFirst()) {
			def = c.getInt(0);
		}
		return def;
	}
}
