package pl.dzienniczekbobasa.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MainTabsAdapter extends FragmentPagerAdapter {

	public MainTabsAdapter(FragmentManager fm) {
		super(fm);
	}
	
	@Override
	public Fragment getItem(int index) {
		switch (index) {
		default: return new TabFoodFragment();
		case 0: return new TabMileFragment();
		case 1: return new TabFoodFragment();
		case 2: return new TabBodyFragment();
		}
	}
	
	
	/**
	 * @return ilo�� zak�adek
	 */
	@Override
	public int getCount() {
		return 3;
	}

}
